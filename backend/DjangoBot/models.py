from django.db import models


class Message(models.Model):
    text = models.TextField(
        blank=False,
        null=False
    )
    time = models.TimeField(
        auto_now=True
    )

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None, *args, **kwargs):
        super().save(*args, **kwargs)
