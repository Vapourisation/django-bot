import json
import os


def filter_input(data):
    bad_words = open('datasets/bad_words.txt', 'r')
    if data in bad_words:
        return "Woops, looks like that's a rude word!"
    else:
        save_corpus(data)


def save_corpus(data):
    with open('bot_corpus.json') as file:
        f = json.load(file)
        for line in f:
            title = line['title']
            lineData = line['data']
        return
